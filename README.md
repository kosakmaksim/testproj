# TestProj



## How to run tests
You can run tests locally or remotly via Git Lab.

Local run:

Required items to install:
    - python:3.8
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install pytest-playwright
    - playwright install

Run all tests via terminal 
    - python -m pytest tests 
Run all tests via terminal in headed mode 
    - python -m pytest tests --headed


