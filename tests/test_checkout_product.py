import pytest
from playwright.sync_api import Page, expect

from src.constants import constants
from src.pages.bucket_page import BucketPage
from src.pages.login_page import LoginPage
from src.pages.main_page import MainPage
from src.utils import get_random_string, get_random_string_only_numbers


@pytest.fixture()
def login(page: Page):
    login_page = LoginPage(page)
    login_page.sign_in(constants.USER, constants.POSITIVE_PASSWORD)


# Verify checkout product
def test_add_to_card(login, page: Page):
    main_page = MainPage(page)
    main_page.add_product_to_card()
    bucket_page = BucketPage(page)
    bucket_page.checkout_product(get_random_string(5), get_random_string(5), get_random_string_only_numbers(5))
    expect(bucket_page.complete_order).to_have_text(constants.COMPLETE_ORDER)
