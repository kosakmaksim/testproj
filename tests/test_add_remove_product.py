import pytest
from playwright.sync_api import Page, expect

from src.constants import constants
from src.pages.bucket_page import BucketPage
from src.pages.login_page import LoginPage
from src.pages.main_page import MainPage


@pytest.fixture()
def login(page: Page):
    login_page = LoginPage(page)
    login_page.sign_in(constants.USER, constants.POSITIVE_PASSWORD)


# Verify product added to cart
def test_add_to_card(login, page: Page):
    main_page = MainPage(page)
    main_page.add_product_to_card()
    bucket_page = BucketPage(page)
    expect(bucket_page.added_product).to_be_visible()


# Verify product removed from cart
def test_remove_from_card(login, page: Page):
    main_page = MainPage(page)
    main_page.add_product_to_card()
    bucket_page = BucketPage(page)
    bucket_page.remove_sauce_labs_backpack_button.click()
    expect(bucket_page.added_product).not_to_be_visible()
