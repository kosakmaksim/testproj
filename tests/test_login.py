from playwright.sync_api import Page, expect

from src.constants import constants
from src.pages.login_page import LoginPage


# Verify successful login
def test_positive_login_flow(page: Page):
    login_page = LoginPage(page)
    login_page.sign_in(constants.USER, constants.POSITIVE_PASSWORD)
    expect(page).to_have_url(constants.MAIN_URL)


# Verify invalid login
def test_negative_login_flow(page: Page):
    login_page = LoginPage(page)
    login_page.sign_in(constants.USER, constants.NEGATIVE_PASSWORD)
    expect(login_page.failed_login_element).to_have_text(constants.FAILED_LOGIN_MSG)
