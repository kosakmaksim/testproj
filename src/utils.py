import random
import string


def get_random_string(length) -> str:
    # letters = string.ascii_lowercase
    result_str = ''.join(random.choice(string.ascii_letters) for i in range(length))
    return result_str


def get_random_string_only_numbers(length) -> str:
    # letters = string.ascii_lowercase
    characters = string.digits
    result_str = ''.join(random.choice(characters) for i in range(length))
    return result_str
