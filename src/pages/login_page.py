class LoginPage:

    def __init__(self, page):
        self.page = page
        self.login_field = page.locator("[id=\"user-name\"]")
        self.password_field = page.locator("[id=\"password\"]")
        self.login_button = page.locator("[id=\"login-button\"]")
        self.failed_login_element = page.locator("div[class=\"error-message-container error\"]")

    def navigate(self):
        self.page.goto("https://saucedemo.com/")

    def sign_in(self, login, password):
        self.navigate()
        self.login_field.fill(login)
        self.password_field.fill(password)
        self.login_button.click()
