class BucketPage:

    def __init__(self, page):
        self.page = page
        self.added_product = page.locator("//div[contains(text(),'Sauce Labs Backpack')]")
        self.remove_sauce_labs_backpack_button = \
            page.locator("//div[contains(text(),'Sauce Labs Backpack')]/../..//button")
        self.checkout_button = page.locator("//button[@id='checkout']")
        self.first_name_field = page.locator("//input[@id='first-name']")
        self.last_name_field = page.locator("//input[@id='last-name']")
        self.zip_code = page.locator("//input[@id='postal-code']")
        self.continue_button = page.locator("//input[@id='continue']")
        self.finish_button = page.locator("//button[@id='finish']")
        self.complete_order = page.locator("//h2[@class='complete-header']")

    def checkout_product(self, first_name, last_name, zip_code):
        self.checkout_button.click()
        self.first_name_field.fill(first_name)
        self.last_name_field.fill(last_name)
        self.zip_code.fill(zip_code)
        self.continue_button.click()
        self.finish_button.click()

