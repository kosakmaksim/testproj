class MainPage:

    def __init__(self, page):
        self.page = page
        self.add_to_card_button = page.locator("[id=\"add-to-cart-sauce-labs-backpack\"]")
        self.remove_from_card_button = page.locator("[id=\"remove-sauce-labs-backpack\"]")
        self.bucket = page.locator("[id=\"shopping_cart_container\"]")

    def add_product_to_card(self):
        self.add_to_card_button.click()
        self.bucket.click()
